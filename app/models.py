# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils.functional import cached_property
from django.core.paginator import Paginator
from django.db import connection, transaction, OperationalError
from django.contrib.postgres.fields import ArrayField

from tinymce.models import HTMLField


class TimeLimitedPaginator(Paginator):
    @cached_property
    def count(self):
        with transaction.atomic(), connection.cursor() as cursor:
            cursor.execute('SET LOCAL statement_timeout TO 200;')
            try:
                return super().count
            except OperationalError:
                return 9999999999



class Authors(models.Model):
    english_name = models.CharField(max_length=255, blank=True, null=True)
    original_name = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'authors'


class Collections(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)
    urn = models.CharField(max_length=255, blank=True, null=True)
    repository = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'collections'

    def __str__(self):
        return f'{self.title}'


class Exemplars(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    urn = models.CharField(max_length=255, blank=True, null=True)
    workid = models.ForeignKey('Works', models.DO_NOTHING, db_column='workId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'exemplars'


class Languages(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'languages'

    def __str__(self):
        return f'{self.title}'


class Refsdecls(models.Model):
    label = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    match_pattern = models.CharField(max_length=255, blank=True, null=True)
    replacement_pattern = models.TextField(blank=True, null=True)
    structure_index = models.IntegerField(blank=True, null=True)
    urn = models.CharField(max_length=255, blank=True, null=True)
    workid = models.ForeignKey('Works', models.DO_NOTHING, db_column='workId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'refsdecls'


class Textgroups(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)
    urn = models.CharField(max_length=255, blank=True, null=True)
    collectionid = models.ForeignKey(Collections, models.DO_NOTHING, db_column='collectionId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'textgroups'

    def __str__(self):
        return f'{self.urn} {self.title}'


class Textnodes(models.Model):
    index = models.IntegerField(blank=True, null=True)
    #location = models.TextField(blank=True, null=True)  # This field type is a guess.
    location =  ArrayField(models.IntegerField(), blank=True, null=True)
    text = HTMLField()
    workid = models.ForeignKey('Works', models.DO_NOTHING, db_column='workId', blank=True, null=True)  # Field name made lowercase.
    field_search = models.TextField(db_column='_search', blank=True, null=True)  # Field renamed because it started with '_'. This field type is a guess.
    normalized_text = models.TextField(blank=True, null=True)

    paginator = TimeLimitedPaginator

    class Meta:
        managed = False
        db_table = 'textnodes'


class Translations(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    urn = models.CharField(max_length=255, blank=True, null=True)
    workid = models.ForeignKey('Works', models.DO_NOTHING, db_column='workId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'translations'


class Versions(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    urn = models.CharField(max_length=255, blank=True, null=True)
    workid = models.ForeignKey('Works', models.DO_NOTHING, db_column='workId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'versions'


class Works(models.Model):
    filemd5hash = models.CharField(max_length=255, blank=True, null=True)
    filename = models.CharField(max_length=255, blank=True, null=True, unique=True)
    original_title = models.CharField(max_length=255, blank=True, null=True)
    english_title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255, blank=True, null=True)
    structure = models.CharField(max_length=255, blank=True, null=True)
    form = models.CharField(max_length=255, blank=True, null=True)
    urn = models.CharField(max_length=255, blank=True, null=True)
    work_type = models.CharField(max_length=255, blank=True, null=True)
    label = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    full_urn = models.CharField(max_length=2048, blank=True, null=True)
    authorid = models.ForeignKey(Authors, models.DO_NOTHING, db_column='authorId', blank=True, null=True)  # Field name made lowercase.
    languageid = models.ForeignKey(Languages, models.DO_NOTHING, db_column='languageId', blank=True, null=True)  # Field name made lowercase.
    textgroupid = models.ForeignKey(Textgroups, models.DO_NOTHING, db_column='textgroupId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'works'

    def __str__(self):
        return f'{self.full_urn}'
