from django.contrib import admin

from .models import Collections, Textgroups, Works, Textnodes, Languages

class CollectionsAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'slug', 'urn', 'repository' ]

class TextgroupsAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'slug', 'urn', 'collectionid' ]

class WorksAdmin(admin.ModelAdmin):
    list_display = ['id', 'english_title', 'original_title', 'slug', 'urn', 'full_urn', 'languageid' ]
    raw_id_fields = ("textgroupid",)

class TextnodesAdmin(admin.ModelAdmin):
    list_display = ['id', 'index', 'location', 'get_text', 'normalized_text',  ]
    raw_id_fields = ("workid",)

    def get_text(self, obj):
        if len(obj.text) > 80:
            short_text = obj.text[:80] + '...'
            return short_text
        else:
            return obj.text
    get_text.short_description = 'Text'

class LanguagesAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'slug']

admin.site.register(Collections, CollectionsAdmin)
admin.site.register(Textgroups, TextgroupsAdmin)
admin.site.register(Works, WorksAdmin)
admin.site.register(Textnodes, TextnodesAdmin)
admin.site.register(Languages, LanguagesAdmin)
